import socket, sys
import select
import time
from struct import *
from random import getrandbits
import os
ICMP_ECHOREPLY = 0
ICMP_ECHO = 8
# checksum functions needed for calculation checksum
def checksum(msg):
    s = 0
     
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = ord(msg[i]) + (ord(msg[i+1]) << 8 )
        s = s + w
     
    s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
     
    #complement and mask to 4 byte short
    s = ~s & 0xffff
     
    return s

class Ipv4_packet:
    """
    class for creating a Ipv4 packet and it's parsing
    """
    def __init__(self, dest_addr='', source_ip='0.0.0.0'):
        self.source_ip = source_ip
        self.dest_ip = socket.gethostbyname(dest_addr)
        # ip header fields
        self.ip_ihl = 5 # internet header length 5*32 = 160bits minimum
        self.ip_ver = 4 # ipv4
        self.ip_tos = 0 # type of service (ToS)
        self.ip_tot_len = 0  # kernel will fill the correct total length
        self.ip_id = 54321   #Id of this packet
        self.ip_frag_off = 0 # Reserved = 0 DF = 0 MF = 0 offset = 0
        self.ip_ttl = 255 
        self.ip_proto = socket.IPPROTO_ICMP
        self.ip_check = 0    # kernel will fill the correct checksum
        self.ip_saddr = socket.inet_aton (self.source_ip)   #Spoof the source ip address if you want to
        self.ip_daddr = socket.inet_aton (self.dest_ip )
        self.ip_ihl_ver = (self.ip_ver << 4) + self.ip_ihl
    def assemble(self):
        """"""
        # the ! in the pack format string means network order
        self.ip_header = pack('!BBHHHBBH4s4s' , self.ip_ihl_ver,
                         self.ip_tos, self.ip_tot_len,
                         self.ip_id, self.ip_frag_off,
                         self.ip_ttl, self.ip_proto,
                         self.ip_check, self.ip_saddr, self.ip_daddr)
        return self.ip_header
    def _disassemble(self, pkt):
        self.ip_header = pkt[0:20]
        iph = unpack('!BBHHHBBH4s4s' , self.ip_header)
        self.ip_ihl_ver = iph[0]
        self.ip_tos = iph[1]
        self.ip_tot_len = iph[2]
        self.ip_id = iph[3]
        self.ip_frag_off = iph[4]
        self.ip_ttl = iph[5]
        self.ip_proto = iph[6]
        self.ip_check = iph[7]
        self.ip_saddr = socket.inet_ntoa(iph[8])
        self.ip_daddr =  socket.inet_ntoa(iph[9])
        self.ip_ver = self.ip_ihl_ver >> 4
        self.ip_ihl = self.ip_ihl_ver & 0xF
        

class IcmpPacket(Ipv4_packet):
    """"""
    def __init__(self, dest_addr='', source_ip='0.0.0.0' , _type=0, code=0, ident=0, seq=0):
        """"""
        Ipv4_packet.__init__(self, dest_addr, source_ip)
        self.type_ = _type
        self.code = code
        self.check_sum = 0
        self.seq_num = seq
        self.identifier = ident
    def make_header(self):
        header = pack('!BBHHH' , self.type_,
                      self.code, self.check_sum,
                      self.identifier, self.seq_num)
        self.check_sum = checksum(header)
        self.icmp_header = pack('!BB' , self.type_, self.code) + pack('H', self.check_sum) + pack('!HH', self.identifier, self.seq_num)
        return self.icmp_header
    def assemple_packet(self):
        ip_header = self.assemble()
        icmp_header = self.make_header()
        return (ip_header + icmp_header)
    def _disassemble(self, pkt):
        Ipv4_packet._disassemble(self, pkt)
        if self.ip_proto == socket.IPPROTO_ICMP:
            packet = pkt[20:28]
            icmp_h = unpack('!BBHHH' , packet)
            self.type_ = icmp_h[0]
            self.code  = icmp_h[1]
            self.checksum  = icmp_h[2]
            self.identifier = icmp_h[3]
            self.seq_num  = icmp_h[4]
        else:
            raise ValueError
        
class PingSocket:
    """"""
    def __init__(self, addr):
        """"""
        self.dest = (socket.gethostbyname(addr), 0) # dummy port aas ICMP doesnot uses a port
        #create a raw socket
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
        except socket.error , msg:
            print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            sys.exit()
        try:
            self.rec = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) #socket.socket( socket.AF_PACKET , socket.SOCK_RAW , socket.ntohs(0x0003))
        except socket.error , msg:
            print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            sys.exit()
        
    def sendto(self, packet):
        self.socket.sendto(packet, self.dest)
    def recvfrom(self, maxbytes):
        return self.rec.recvfrom(maxbytes)


class Pinger:
    """"""
    def __init__(self, addr, num):
        """"""
        self.num = num
        self.addr = addr
        self.last = 0
        self.send = 0
        self.times = {}
        self.deltas = []
        self.sock = PingSocket(addr)
        self.iden = getrandbits(16)
        self.source = '0.0.0.0'
        self.dest_addr = addr
        name, aliases, ipaddr = socket.gethostbyaddr(addr)
        self.destinfo = (name, ipaddr[0])
        
    def send_packet(self):
        pkt = IcmpPacket(self.dest_addr, self.source , ICMP_ECHO, 0 , self.iden , self.send)
        buf = pkt.assemple_packet()
        self.times[self.send] = time.time()
        self.sock.sendto(buf)
        self.plen = len(buf)
        self.send = self.send + 1
    def recv_packet(self, pkt, when, length):
        try:
            send = self.times[pkt.seq_num]
            del self.times[pkt.seq_num]
        except KeyError:
            return 
        delta = int((when - send)*1000.0)
        self.deltas.append(delta)
        print "{} bytes from {} ({}): icmp_seq={}. time={}. ms".format(length, self.destinfo[0],
                                                                       self.destinfo[1], pkt.seq_num,
                                                                       delta)
        if pkt.seq_num > self.last:
            self.last = pkt.seq_num
    def ping(self):
        """"""
        self.last_arrival = time.time()
        while True:
            if self.send < self.num:
                self.send_packet()
            elif not self.times and self.last == self.num - 1:
                break
            else:
                now = time.time()
                if self.deltas:
                    # Wait no more than 10 times the longest delay so far
                    if (now - self.last_arrival) > max(self.deltas) / 100.:
                        break
                else:
                    # Wait no more than 10 seconds
                    if (now - self.last_arrival) > 10.:
                        break
            self.wait()
    def wait(self):
        start = time.time()
        timeout = 1.0
        while True:
            rd, wt, er = select.select([self.sock.rec], [], [], timeout)
            timeout = (start + 1.0) - time.time()
            if timeout < 0.0:
                break
            if rd:
                # okay to use time here, because select has told us
                # there is data and we don't care to measure the time
                # it takes the system to give us the packet.
                arrival = time.time()
                try:
                    pkt, who = self.sock.recvfrom(4096)
                except socket.error:
                    continue
                # could also use the ip module to get the payload
                try:
                    reply, length = disassemble(pkt)
                except ValueError:
                    continue
                if reply.type_ == ICMP_ECHOREPLY and reply.identifier == self.iden:
                    self.recv_packet(reply, arrival, length)
                    self.last_arrival = arrival
            else:
                print "Packet timeout"
                        
    def get_summary(self):
        if self.deltas:
            dmin = min(self.deltas)
            dmax = max(self.deltas)
            davg = reduce(lambda x, y: x + y, self.deltas) / len(self.deltas)
        else:
            dmin = 0
            dmax = 0
            davg = 0
        sent = self.num
        recv = sent - len(self.times.values())
        try:
            loss = float(sent - recv) / float(sent)
        except:
            loss = 0
        return dmin, davg, dmax, sent, recv, loss
def disassemble(pkt):
    """ disassemble pack to remove or get values of header"""
    packet = IcmpPacket()
    packet._disassemble(pkt)
    return (packet, len(pkt))
    
import string
if __name__ == '__main__':
    try:
        who = sys.argv[1]
    except IndexError:
        print "ping.py host [#packets]"
        sys.exit(0)
    try:
        num = string.atoi(sys.argv[2])
    except ValueError:
        print "ping.py host [#packets]"
        sys.exit(0)
    except IndexError:
        num = 32
    p = Pinger(who, num)
    print "PING {} ({})".format(who, p.destinfo[1])
    p.ping()
    summary = p.get_summary()
    print "---Ping statistics---"
    print "%d packets transmitted, %d packets received, %d%% packet loss" % \
          (summary[3], summary[4], int(summary[5] * 100.))
    print "round-trip (ms)   min/avg/max = %d/%d/%d" % \
          (summary[0], summary[1], summary[2])
