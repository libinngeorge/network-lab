from struct import pack, unpack
import socket, sys

def checksum(msg):
    s = 0
    data_len = len(msg)
    if (data_len%2) == 1:
        data_len += 1
        msg += pack('!B', 0)
    s = 0
     
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = ord(msg[i]) + (ord(msg[i+1]) << 8 )
        s = s + w
     
    s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
     
    #complement and mask to 4 byte short
    s = ~s & 0xffff
     
    return s

class Ipv4_packet:
    """
    class for creating a Ipv4 packet and it's parsing
    """
    def __init__(self, dest_addr='', source_ip='0.0.0.0', protocol = socket.IPPROTO_UDP, data=''):
        self.source_ip = socket.gethostbyname(source_ip)
        self.dest_ip = socket.gethostbyname(dest_addr)
        # ip header fields
        self.ip_ihl = 5 # internet header length 5*32 = 160bits minimum
        self.ip_ver = 4 # ipv4
        self.ip_tos = 0 # type of service (ToS)
        self.ip_tot_len = 0  # kernel will fill the correct total length
        self.ip_id = 54321   #Id of this packet
        self.ip_frag_off = 0 # Reserved = 0 DF = 0 MF = 0 offset = 0
        self.ip_ttl = 255 
        self.ip_proto = protocol
        self.ip_check = 0    # kernel will fill the correct checksum
        self.ip_saddr = socket.inet_aton (self.source_ip)   #Spoof the source ip address if you want to
        self.ip_daddr = socket.inet_aton (self.dest_ip )
        self.ip_ihl_ver = (self.ip_ver << 4) + self.ip_ihl
        self.data = data
    def _assemble(self):
        """ Assamble the IPV4 packet """
        # the ! in the pack format string means network order (Big endian)
        self.ip_header = pack('!BBHHHBBH4s4s' , self.ip_ihl_ver,
                         self.ip_tos, self.ip_tot_len,
                         self.ip_id, self.ip_frag_off,
                         self.ip_ttl, self.ip_proto,
                         self.ip_check, self.ip_saddr, self.ip_daddr)
        return self.ip_header + self.data
    def _disassemble(self, pkt):
        """
        disassamble the IPV4 packet to parse its header
        """
        self.ip_header = pkt[0:20]
        iph = unpack('!BBHHHBBH4s4s' , self.ip_header)
        self.ip_ihl_ver = iph[0]
        self.ip_tos = iph[1]
        self.ip_tot_len = iph[2]
        self.ip_id = iph[3]
        self.ip_frag_off = iph[4]
        self.ip_ttl = iph[5]
        self.ip_proto = iph[6]
        self.ip_check = iph[7]
        self.ip_saddr = socket.inet_ntoa(iph[8])
        self.ip_daddr =  socket.inet_ntoa(iph[9])
        self.ip_ver = self.ip_ihl_ver >> 4
        self.ip_ihl = self.ip_ihl_ver & 0xF
        self.data = pkt[20:]

class UDP_Packet(Ipv4_packet):
    """
    class for creating a UDP packet and it's parsing
    """
    def __init__(self, dest=('0.0.0.0', 0), message='', source=('0.0.0.0', 0)):
        self.source_ip, self.source_port = (socket.gethostbyname(source[0]), source[1])
        self.dest_ip, self.dest_port = (socket.gethostbyname(dest[0]), dest[1])
        Ipv4_packet.__init__(self, self.dest_ip, self.source_ip)
        self.length = 0
        self.check_udp = 0
        self.message = message
        self.udp_header = ''
    def _assemble(self):
        """
        assamble UDP packet 
        calculates UDP checksum 
        """
        source_ip =  socket.inet_aton(self.source_ip)
        dest_ip =  socket.inet_aton(self.dest_ip)
        if type(self.message) != bytes:
            self.message = bytes(self.message.encode('utf-8'))
        self.length = 8 + len(self.message)
        pseudo_header = source_ip +  dest_ip + pack('!BBH', 0, socket.IPPROTO_UDP, self.length)
        udp_header = pack('!4H',  self.source_port, self.dest_port, self.length, self.check_udp)
        self.check_udp = checksum(pseudo_header + udp_header + self.message)
        self.udp_header = pack('!HHH' , self.source_port, self.dest_port, self.length) + pack('H', self.check_udp)
        self.data = self.udp_header + self.message
        data = Ipv4_packet._assemble(self)
        return data
    def _disassemple(self, pkt, flag):
        """
        flag set to false if pkt doesnot contain any header 
        """
        if flag:
            Ipv4_packet._disassemble(self, pkt)
            if self.ip_proto == socket.IPPROTO_UDP:
                udp_h = unpack('!HHHH' , self.data[0:8])
                self.source_port = udp_h[0]
                self.dest_port = udp_h[1]
                self.length = udp_h[2]
                self.check_udp = udp_h[3]
                self.message = self.data[8:]
            else:
                raise KeyError
        else:
            self.message = pkt
        
class UDPSocket:
    """
    UDP socket class for send, recive packets
    """
    def __init__(self, addr, port):
        """ 
        create a raw socket
        """
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW) # RAW SOCKET 
        except socket.error , msg:
            print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            sys.exit()
        try:
            self.rec = sock = socket.socket(socket.AF_INET, # Internet
                       						socket.SOCK_DGRAM) # UDP SOCKET FOR RECIVING PACKET
  #socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
  #socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
  #socket.socket( socket.AF_PACKET , socket.SOCK_RAW , socket.ntohs(0x0003))
        except socket.error , msg:
            print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            sys.exit()
        self.rec.bind((addr, port)) 
    def sendto(self, packet, dest):
        """ Send Packet to dest 
        dest -> (ip, port)
        """
        self.socket.sendto(packet, dest)
    def recvfrom(self, maxbytes):
        """ 
        Recive Packet 
        """
        return self.rec.recvfrom(maxbytes)


if __name__ == '__main__':
    server_ip = [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
    try:
        server_port =  int(sys.argv[1])
    except:
        print "Usage: python pongServer.py <port>"
        sys.exit()
    UDP_s = UDPSocket(server_ip, server_port)
    try:
        while True:
            rec = UDP_Packet()
            print "receving packets ..... "
            pkt, ip = UDP_s.recvfrom(1024)
            try:
                print "recived message = {} from {}".format(pkt, ip)
                rec._disassemple(pkt, False)
            except:
                continue
            pac = UDP_Packet(ip, "PONG\n", (server_ip, server_port))
            pkt = pac._assemble()
            UDP_s.sendto(pkt, ip)
            print "pong packet send to {}".format(ip)
            if rec.message == 'exit\n':
                break
    finally:
        print "Closing Sockets"
        UDP_s.socket.close()
        UDP_s.rec.close()
        
    
