import socket
import sys

# Create a UDP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    ip = sys.argv[1]
    port = int(sys.argv[2])
except:
    print "Usage: python pongClient.py <host> <port>"
    sys.exit()
# Bind the socket to the port
server_address = (ip, port)
print 'starting up on {} port {}'.format(ip, port)
message = "Hi, who is there"

try:
    while True:
        # Send data
        print 'sending {}'.format(message)
        sent = sock.sendto(message, server_address)
        # Receive response
        print 'waiting to receive'
        data, server = sock.recvfrom(4096)
        print 'received {}'.format(data)
finally:
    print 'closing socket'
    sock.close()
