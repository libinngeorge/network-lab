import hashlib
import random
import sys

class Lamport:
    """"""
    def __init__(self, public_key_file=None):
        """ Class constructor 
        public_key is not none then signing is not possible
        """
        self.verifiable = True
        if public_key_file:
            with open(public_key_file) as f:
                self.public = f.read().splitlines()
            self.signable=False
            self.private = []
        else:
            self.public, self.private = self.generate_public_private_keys(512)
            self.write_file(self.public, "lamport.pub")
            self.write_file(self.private, "lamport.key")
            self.signable=True
        self.used = False
        
    
    def generate_public_private_keys(self, message_len=512):
        """Generate private key and public key for lamport 
        for a given message length(default 512)
        USED FOR SIGNING """
        private = []
        public = []
        for i in range(message_len*2):
            r = random.randint(0, 2**message_len - 1)
            pri = hashlib.sha512(str(r)).hexdigest()
            pub = hashlib.sha512(str(int(pri, 16))).hexdigest()
            public.append(pub)
            private.append(pri)
        return (public, private)
    def write_file(self, key, filename):
        """Write array(key) to the file 'filename'"""
        with open(filename, "w") as f:
            for i in key:
                f.write(i)
                f.write("\n")
    def sign_document(self, document_name):
        """Signs the document"""
        if self.used:
            print "Warning: this private key is already used for Signing another document"
        if not self.signable:
            print "This Lamport Class doesnot have the private key for signing document"
            return None
        else:
            sign = []
            with open(document_name) as f:
                data = f.read()
                hashed_data = hashlib.sha512(data).hexdigest()
                bits =  "{0:0>512b}".format(int(hashed_data, 16))
                j = 0
                for i in bits:
                    sign.append(self.private[j*2 + int(i)])
                    j+=1
            self.used = True
            return sign
    def verfy_document_sign(self, document, sign_file):
        """ Verify document given signature and document 
        Possible only if public key is available in the Lamport class"""
        if self.verifiable:
            with open(document) as f:
                data = f.read()
                hashed_data = hashlib.sha512(data).hexdigest()
                bits =  "{0:0>512b}".format(int(hashed_data, 16))
            with open(sign_file) as sf:
                sign = sf.read().splitlines() 
            if len(sign) != len(bits):
                print "Error in signature file format"
                return False
            j = 0
            for s, i in zip(sign, bits):
                pub = hashlib.sha512(str(int(s, 16))).hexdigest()
                if (pub ==self.public[j*2 + int(i)]):
                    j+=1
                    continue
                else:
                    return False
            return True
        else:
            print "Public key not found"
            return True

def main(document, publickey = None):
    a = Lamport(publickey)
    sign_file = raw_input("Enter file name for the document signature : ")
    if not publickey:
        sign = a.sign_document(document)
        with open(sign_file, "w") as f:
            for i in sign:
                f.write(i)
                f.write("\n")
        print "SIGNATURE GENERATED FOR THE GIVEN DOCUMENT"
    if a.verfy_document_sign(document, sign_file):
        print "VERIFICATION SUCESSFULL"
    else:
        print "VERIFICATION FAILED"

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: python Lamport.py <document file for verification/ signing> <publickey file if available for verification>"
        sys.exit()
    try:
        publickey = sys.argv[2]
    except:
        publickey = None
    main(sys.argv[1],publickey)
