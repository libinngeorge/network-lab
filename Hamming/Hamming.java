/*
 Hamming Class implements Encoding
 and Decoding of Hamming code for a given r
*/
import java.io.*;
import java.util.*;
import java.lang.Math;

public class Hamming {
    static int r;
    static int in_len;
    static int out_len;
    private int[][] H; 
    private Dictionary <String,String>codeBook;
    
    public Hamming(int R){
	r = R;
	in_len = (int)(Math.pow(2,r)) - r - 1;
	out_len = (int)(Math.pow(2,r)) - 1;
	H = new int[r][out_len];
	codeBook = new Hashtable<String,String>();
	int t;
	int[] in_code = new int[in_len];
	int[] out_code;
	String Codeword;
	String code_dec;
	for( int i = 0;i < Math.pow(2,in_len); i++){
	    t = i + 1;
	    for( int k = 0;k < in_len; k++){
		if ((t % 2) != 0){
		    in_code[k] = 1;
		}
		else{
		    in_code[k] = 0;
		}
		t = t/2;
	    }
	    out_code = Encoder(in_code);
	    Codeword = Arrays.toString(out_code).replaceAll("\\[|\\]|,|\\s", "");
	    code_dec = Arrays.toString(in_code).replaceAll("\\[|\\]|,|\\s", "");
	    codeBook.put(Codeword, code_dec);
	}
	CreateHMatrix();
    }
    
    public String Decoder(int[] code){
	String decode;
	int[] error = new int[r];
	int error_index = 0;
	int input = 0;
	for( int j = 0;j < r; j++){
	    for( int i = 0;i < out_len; i++){
		error[j] = error[j] + code[i]*H[j][i];
	    }
	    error[j] = error[j] % 2;
	}
	for( int j = 0;j < r; j++){
	    if(error[j] != 0){
		error_index = error_index + (int)Math.pow(2, j);
	    }
	}
	if( error_index != 0){
	    code[error_index - 1] = (code[error_index - 1] + 1) % 2;
	}
	String Codeword = Arrays.toString(code).replaceAll("\\[|\\]|,|\\s", "");
	decode = codeBook.get(Codeword);
	System.out.println(decode);
	return decode;
    }
    
    private void CreateHMatrix(){
	int t;
	for( int j = 0;j < out_len; j++){
	    t = j + 1;
       	    for( int i = 0;i < r; i++){
		if ((t % 2) != 0){
		    H[i][j] = 1;
		}
		t = t/2;
	    }
	}
    }
    
    public int[] Encoder(int[] input) {
	int[] code = new int[out_len];
	int i = in_len - 1;
	int f = 1;
	for( int j = 0;j < out_len ; j++){
	    if (((j+1) % f) == 0) {
		f = f * 2;
		continue;
	    }
	    else {
		code[j] = input[i--];
	    }
	}
	for (i = 1; i <= out_len; i=i*2){
	    code[i-1] = compute_parity(i, code);
	}
	return code;
    }
    
    private int compute_parity(int F, int[] code){
	int parity = 0;
	for (int i = F; i <= out_len; i = i + 2*F){
	    for (int j = 0; j < F; j++){
		parity = parity + code[i-1 + j];
	    }
	}
	return (parity % 2);
    }
    
    public int[] ParseBinCode(String b) {
	int[] encode = new int[in_len];
	if (b.length() != in_len) {
	    System.out.println("Code Word should have a length of " + in_len + "\n");
	    return null;
	}
	for (int i = 0;i < b.length(); i++){
	    if (b.charAt(i)=='1'){
		encode[i] = 1;
	    }
	    else if (b.charAt(i)=='0'){
		encode[i] = 0;
	    }
	    else{
		System.out.println("Invalid char in the input string\n" + b.charAt(i));
		return null;
	    }
	}
	return encode;
    }
    public static void main(String[] args) throws IOException {
	System.out.println("Enter r for Hamming Code\n");
	BufferedReader reader = 
                   new BufferedReader(new InputStreamReader(System.in));
	int r_ = Integer.parseInt(reader.readLine());
	Hamming Enc = new Hamming(r_);
	System.out.println("Enter r for Hamming Code of length " + Enc.in_len + "\n");
	String b;
	int[] input;
	do {
	    b = reader.readLine();
	    input = Enc.ParseBinCode(b);
	} while (input == null);
	int[] code = Enc.Encoder(input);
	System.out.println("Encoded Output: " + Arrays.toString(code).replaceAll("\\[|\\]|,|\\s", ""));
	Random rand = new Random();
	int  n = rand.nextInt(Enc.out_len);
	code[n] = (code[n] + 1) % 2;
	System.out.println("Encoded Output with one bit error : "+ Arrays.toString(code).replaceAll("\\[|\\]|,|\\s", ""));
	String decode = Enc.Decoder(code);
	System.out.println("Decoded Output: " + decode);
    }   
}
