/*
 Hamming Class implements Encoding
 and Decoding of Dual Hamming code for a given r
*/
import java.io.*;
import java.util.*;
import java.lang.Math;

public class DualHamming {
    static int in_len;
    static int out_len;
    private int[][] H;
    private int[][] G; 
    private Dictionary <String,String>codeBook;
    static Dictionary <String,String>syndromeMap;
    
    public DualHamming(int R){
	in_len = R;
	out_len = (int)(Math.pow(2,R)) - 1;
	G = new int[in_len][out_len];
	H = new int[out_len - in_len][out_len];
	codeBook = new Hashtable<String,String>();
	syndromeMap = new Hashtable<String,String>();
	int t;
	int[] in_code = new int[in_len];
	int[] out_code;
	String Codeword;
	String code_dec;
	CreateGMatrix();
	for( int i = 0;i < Math.pow(2,in_len); i++){
	    t = i + 1;
	    for( int k = 0;k < in_len; k++){
		if ((t % 2) != 0){
		    in_code[k] = 1;
		}
		else{
		    in_code[k] = 0;
		}
		t = t/2;
	    }
	    out_code = Encoder(in_code);
	    Codeword = Arrays.toString(out_code).replaceAll("\\[|\\]|,|\\s", "");
	    code_dec = Arrays.toString(in_code).replaceAll("\\[|\\]|,|\\s", "");
	    codeBook.put(Codeword, code_dec);
	}
	int []new_corr = new int[out_len];;
	int []corr;
	String S_corr;
	String syndrome;
	for( int i = 0;i < Math.pow(2,out_len); i++){
	    t = i + 1;
	    for( int k = 0;k < out_len; k++){
		if ((t % 2) != 0){
		    new_corr[k] = 1;
		}
		else{
		    new_corr[k] = 0;
		}
		t = t/2;
	    }
	    syndrome = Arrays.toString(GetSyndrome(new_corr)).replaceAll("\\[|\\]|,|\\s", "");//Decoder(new_corr);
	    S_corr = syndromeMap.get(syndrome);
	    int old_weight = 0;
	    int new_weight = 0;
	    String n_corr = Arrays.toString(new_corr).replaceAll("\\[|\\]|,|\\s", "");//ParseBinCode(, out_len);
	    if(S_corr != null){
		corr = ParseBinCode(S_corr, out_len);
		for( int l = 0;l < S_corr.length(); l++){
		    old_weight = old_weight + corr[l]; 
		}
	    }
	    else{
		old_weight = (int) Math.pow(2, in_len -1);
	    }
	    for( int l = 0;l < n_corr.length(); l++){
		new_weight = new_weight + new_corr[l]; 
	    }
	    if(new_weight < old_weight && new_weight < (Math.pow(2, in_len -2) - 0.5)){
		syndromeMap.put(syndrome, n_corr);
	    }	    
	}
    }
    private static String repeat(String str, int times) {
        return new String(new char[times]).replace("\0", str);
    }
    public String Decoder(int[] code){
	String decode;
	int error_index = 0;
	int input = 0;
	String corr;
	int[] _corr;
	int[] Syn = GetSyndrome(code);
	String s = Arrays.toString(Syn).replaceAll("\\[|\\]|,|\\s", "");
	if(! s.equals(repeat("0", out_len - in_len))){
	    corr = syndromeMap.get(s);
	    if (corr == null){
	    	return null;
	    }
	    _corr = ParseBinCode(corr, corr.length());
	    for( int j = 0;j < out_len; j++){
		code[j] = (code[j] + _corr[j]) % 2;
	    }
	}
	String Codeword = Arrays.toString(code).replaceAll("\\[|\\]|,|\\s", "");
	decode = codeBook.get(Codeword);
	return decode;
    }
    public int[] GetSyndrome(int[] code){
	int r = out_len - in_len;
	int[] error = new int[r];
	for( int j = 0;j < r; j++){
	    for( int i = 0;i < out_len; i++){
		error[j] = error[j] + code[i]*H[j][i];
	    }
	    error[j] = error[j] % 2;
	}
	return error;
    }
    private void CreateGMatrix(){
	int t;
	int d= out_len - in_len;
	for( int j = 0;j < in_len; j++){
       	    for( int i = 0;i < in_len; i++){
		if (i == j){
		    G[i][j] = 1;
		}
		else{
		    G[i][j] = 0;
		}
	    }
	}
	for( int j = in_len;j < out_len; j++){
       	    for( int i = 0;i < d; i++){
		if (i == (j-in_len)){
		    H[i][j] = 1;
		}
		else{
		    H[i][j] = 0;
		}
	    }
	}
	int w = 3;
	int F = 4;
	int k = 0;
	for( int j = in_len ;j < out_len; j++){
	    if (w == F){
		w++;
		F = F*2;
	    }
	    t =w;
       	    for( int i = 0;i < in_len; i++){
		if ((t % 2) != 0){
		    G[i][j] = 1;
		    H[k][i] = 1;
		}
		t = t/2;
	    }
	    k++;
	    w++;
	}
    }
    
    public int[] Encoder(int[] input) {
	int[] code = new int[out_len];
	int f = 1;
	for( int j = 0;j < out_len ; j++){
	    for( int i = 0;i < in_len ; i++){
		code[j] = code[j] + G[i][j]*input[i];
	    }
	    code[j] = code[j] % 2 ;
	}
	return code;
    }
    
    public int[] ParseBinCode(String b, int len) {
	int[] encode = new int[len];
	if (b.length() != len) {
	    System.out.println("Code Word should have a length of " + in_len + "\n");
	    return null;
	}
	for (int i = 0;i < b.length(); i++){
	    if (b.charAt(i)=='1'){
		encode[i] = 1;
	    }
	    else if (b.charAt(i)=='0'){
		encode[i] = 0;
	    }
	    else{
		System.out.println("Invalid char in the input string\n" + b.charAt(i));
		return null;
	    }
	}
	return encode;
    }
    public static void main(String[] args) throws IOException {
	System.out.println("Enter r for Dual Hamming Code\n");
	BufferedReader reader = 
                   new BufferedReader(new InputStreamReader(System.in));
	int r_ = Integer.parseInt(reader.readLine());
	DualHamming Enc = new DualHamming(r_);
	System.out.println("Enter code for Dual Hamming Code of length " + Enc.in_len + "\n");
	String b;
	int[] input;
	do {
	    b = reader.readLine();
	    input = Enc.ParseBinCode(b, Enc.in_len);
	} while (input == null);
	int[] code = Enc.Encoder(input);
	System.out.println("Encoded Output: " + Arrays.toString(code).replaceAll("\\[|\\]|,|\\s", ""));
	int n, no_err;
	System.out.println("Enter number of error to include: ");
	no_err = Integer.parseInt(reader.readLine());
	Random rand = new Random();
	for(int i=0; i< no_err; i++)
	{
		n = rand.nextInt(Enc.out_len);
		code[n] = (code[n] + 1) % 2;
	}
	System.out.println("Encoded Output with " + no_err + " bit error : "+ Arrays.toString(code).replaceAll("\\[|\\]|,|\\s", ""));
	String decode = Enc.Decoder(code);
	if (decode == null){
		System.out.println("Error cannot be corrected");	
	}
	else {
		System.out.println("Decoded Output: " + decode);
	}
    }   
}
