import java.io.*;
import java.net.*;
import java.lang.Thread;

public class Netcat{
    public static void main(String[] args) throws IOException {
	if (args.length != 2){
	    System.err.println("Usage: java Netcat <host> <port>");
	    System.exit(1);
	}
	String hostName = args[0];
	int portNumber = Integer.parseInt(args[1]);
	try (
	     Socket openSocket = new Socket(hostName, portNumber);
	     PrintWriter out = new PrintWriter(openSocket.getOutputStream(), true);
	     BufferedReader stdIn = new BufferedReader(new InputStreamReader (System.in))
	     ){
	    ServerReplay t1 = new ServerReplay(openSocket);
	    t1.start();
	    String userInput;
	    while (t1.isAlive() && (userInput = stdIn.readLine()) != null){
	    	out.println(userInput);
	    }
	    try { openSocket.close(); }
	    catch (Exception e ){ ; }
	} catch (Exception e) {
	    System.out.println("Error");
	} 
    }
}
class ServerReplay extends Thread{
    public Socket openSocket;
    ServerReplay (Socket in) {
	this.openSocket = in;
    }
    public void run(){
	try (
	     BufferedReader in = new BufferedReader(new InputStreamReader (openSocket.getInputStream()));
	     ){
	    String serverReplay;
	    while ((serverReplay = in.readLine()) != null){
		System.out.println(serverReplay);
	    }
	}catch (Exception e){
	    System.out.println("Error");
	}
    }
}
