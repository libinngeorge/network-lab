import java.io.*;
import java.net.*;
import java.lang.Thread;

public class EchoServer {
    public static void main (String[] args) {
	if (args.length != 1){
	    System.err.println("Usage: java EchoServer <port>");
	    System.exit(1);
	}
	int portNumber = Integer.parseInt(args[0]);
	try {
	    ServerSocket server = new ServerSocket(portNumber);
	    while (true) {
		Socket client = server.accept();
		EchoHandler handler = new EchoHandler(client);
		handler.start();
	    }
	}
	catch (Exception e) {
	    System.err.println("Exception caught:" + e);
	}
    }
}

class EchoHandler extends Thread {
    Socket client;
    InetAddress ClientIP;
    EchoHandler (Socket client) {
	this.client = client;
	this.ClientIP = client.getInetAddress();
    }
    
    public void run () {
	try {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
	    PrintWriter writer = new PrintWriter(client.getOutputStream(), true);
	    writer.println("[type 'bye' to disconnect]");
	    System.out.println("Client "+ ClientIP +" Connected");
	    while (true) {
		String line = reader.readLine();
		if (line.trim().equals("bye")) {
		    writer.println("bye!");
		    System.out.println("Client " + ClientIP + " Disconnected");
		    break;
		}
		writer.println("[echo] " + line);
		System.err.println("[message recived] " + line);
	    }
	}
	catch (Exception e) {
	    System.err.println("Exception caught: client" + ClientIP + " disconnected.");
	}
	finally {
	    try { client.close(); }
	    catch (Exception e ){ ; }
	}
    }
}
