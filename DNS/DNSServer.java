import java.io.*;
import java.net.*;
import java.util.ArrayList;

class DNSServer
{
    public static void main(String args[]) throws Exception {
	int DNS_SERVER_PORT = 53;
	if (args.length == 1){
	    DNS_SERVER_PORT = Integer.valueOf(args[0]);
	}
	DatagramSocket serverSocket = new DatagramSocket(DNS_SERVER_PORT);
	byte[] buf = new byte[512];
	while(true) {
	    DatagramPacket apacket = new DatagramPacket(buf, buf.length);
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    DataOutputStream dos = new DataOutputStream(baos);
	    serverSocket.receive(apacket);
	    System.out.println("\n\nReceived: " + apacket.getLength() + " bytes");
	    int port = apacket.getPort();
	    InetAddress IPAddress = apacket.getAddress();
	    for (int i = 0; i < apacket.getLength(); i++) {
		System.out.print(" 0x" + String.format("%x", buf[i]) + " " );
	    }
	    System.out.println("\n");
	    DataInputStream din = new DataInputStream(new ByteArrayInputStream(buf));
	    Short transaction_id = din.readShort();
	    System.out.println("Transaction ID: 0x" + String.format("%x", transaction_id));
	    Short flags = din.readShort();
	    System.out.println("Flags: 0x" + String.format("%x", flags));
	    Short num_questions = din.readShort();
	    System.out.println("Questions: 0x" + String.format("%x", num_questions));
	    Short num_ans = din.readShort();
	    System.out.println("Answers RRs: 0x" + String.format("%x", num_ans));
	    Short authority_rec_count = din.readShort();
	    System.out.println("Authority RRs: 0x" + String.format("%x", authority_rec_count));
	    Short additional_rec_count = din.readShort();
	    System.out.println("Additional RRs: 0x" + String.format("%x", additional_rec_count));
	    ArrayList <String> QName= new ArrayList <String> ();
	    int recLen = 0;
	    while ((recLen = din.readByte()) > 0) {
		byte[] record = new byte[recLen];
		for (int i = 0; i < recLen; i++) {
		    record[i] = din.readByte();
		}
		QName.add(new String(record, "UTF-8"));
		System.out.println("Record: " + new String(record, "UTF-8"));
	    }
	    Short Qtype = din.readShort();
	    System.out.println("Record Type: 0x" + String.format("%x", Qtype));
	    Short Qclass = din.readShort();
	    System.out.println("Class: 0x" + String.format("%x", Qclass));
	    int found = 1;
	    int numParts = QName.size();
	    String[] domainJamesBond = {"www","james", "bond"};
	    for (int i = numParts-1 ,k = 2 ; (i > 0 || k >= 0); i--, k--){
		if ((QName.get(i)).equals(domainJamesBond[k])) {
		    continue;
		}
		found = 0;
		break;
	    }
	    // HEADER SECTION
	    dos.writeShort(transaction_id);
	    if (found == 1) {
		dos.writeShort(0x8400); // flags 1 0000 1 0 0 0 000 0000
		/*
		  Flags
		  Query/Response Flag = 1 (1 bit)
		  Opcode = 0 QUERY (4 bits)
		  Authoritative Answer Flag = 1 
		  Truncation Flag = 0
		  Recursion Desired = 0
		  Recursion Available = 0
		  reserved = 000
		  RCode = 0000 NOERROR
		 */
	    }
	    else {
		dos.writeShort(0x8003);// flags 1 0000 0 0 0 0 000 0011 NAME ERROR
	    }
	    dos.writeShort(num_questions);
	    if (found == 1) {
		dos.writeShort(0x1); // number of answer records;
	    }
	    else {
		dos.writeShort(0x0); // number of answer records;
	    }
	    dos.writeShort(0x0); // Authority record number
	    dos.writeShort(0x0); // Additional record number
	    // Question Section
	    for (String domainPart : QName) {
		byte[] domainBytes = domainPart.getBytes("UTF-8");
		dos.writeByte(domainBytes.length);
		dos.write(domainBytes);
	    }
	    dos.writeByte(0x0); // to show end of QName
	    dos.writeShort(Qtype); 
	    dos.writeShort(Qclass);
	    //Answer Section
	    if (found == 1) {
		dos.writeShort(0xc00c); //NAME or FIELD
		dos.writeShort(0x0001); // TYPE (1 => An A record for the domain name)
		dos.writeShort(0x0001); // CLASS IN (INternet)
		dos.writeInt(0x0); // TTL = 0 (should not be cached)
		dos.writeShort(0x4); // RDLENGTH
		// IP Address
		dos.writeByte(0x7);
		dos.writeByte(0x7);
		dos.writeByte(0x7);
		dos.writeByte(0x7);
		System.out.println("www.james.bond found\n");
	    }
	    else {
		System.out.println("domain not found\n");
	    }
	    byte[] dnsFrame = baos.toByteArray();
	    DatagramPacket dnsReplayPacket = new DatagramPacket(dnsFrame, dnsFrame.length, IPAddress, port);
	    serverSocket.send(dnsReplayPacket);
	    System.out.println("Send DNS Replay\n");
	}
    }
}
